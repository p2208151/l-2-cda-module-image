#include "Image.h"
#include <iostream>
#include <cassert>
using namespace std;

Image::Image()
{
    hauteur = 0;
    largeur = 0;
}

Image::Image(int hauteur_img, int largeur_img)
{
    assert(largeur_img > 0 && hauteur_img > 0);
    hauteur = hauteur_img;
    largeur = largeur_img;
    tableau = new Pixel_img[largeur * hauteur];
}

Image::~Image() {
    delete[] tableau;
}

bool Image::estValide(int posX, int posY) const
{
    return posX <= largeur && posY <= hauteur;
}

Pixel_img Image::getPix(int posX, int posY)
{
    assert(estValide(posX, posY));
    return tableau[posY * largeur + posX];
}

Pixel_img Image::getPix(int posX, int posY) const {
    assert(estValide(posX, posY));
    return Pixel_img(tableau[posY * largeur + posX]);
}

void Image::setPix(int posX, int posY, Pixel_img color)
{
    assert(estValide(posX, posY));
    tableau[posY * largeur + posX] = color;
}

void Image::dessinerRectangle(int posX_min, int posX_max, int posY_min,int posY_max, Pixel_img color)
{
    int i = posX_min;
    while (i < posX_max) {
        int j = posY_min;
        while (j < posY_max) {
            setPix(i, j, color);
            ++j;
        }
        ++i;
    }
}

void Image::
    dessinerRectangle(0, largeur,0, hauteur, color);
}

void Image::testRegression()
{
    Pixel_img pix = Pixel_img(255, 255, 255);
    Image img = Image(10, 10);
    img.dessinerRectangle(0, 7, 0, 7, pix);
    int i = 0;
    while (i < 4) {
        int j = 0;
        while (j < 4) {
            assert(img.getPix(i, j).r == 255 && img.getPix(i, j).g == 255 && img.getPix(i, j).b == 255);
            ++j;
        }
        ++i;
    }
    img.setPix(0, 0, Pixel_img());
    assert(img.getPix(0, 0).r == 0 && img.getPix(0, 0).g == 0 && img.getPix(0, 0).b == 0);
}

void Image::sauver(const string &nomFichier) const
{
    ofstream fichier(nomFichier.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << hauteur << " " << largeur << endl;
    fichier << "255" << endl;
    int i = 0;
    while (i < hauteur) {
        int j = 0;
        while (j < largeur) {
            const Pixel_img &pix = getPix(j, i);
            fichier << +pix.rouge << " " << +pix.vert << " " << +pix.bleu << " ";
            ++j;
        }
        ++i;
    }

    fichier.close();
}

void Image::ouvrir(const string &nomFichier)
{
    ifstream fichier(nomFichier.c_str());
    assert(fichier.is_open());
    int r, g, b;
    string chaine;
    hauteur = 0;
    largeur = 0;
    fichier >> chaine >> hauteur >> largeur >> chaine;
    assert(hauteur > 0 && largeur > 0);
    if (tableau != nullptr)
        delete[] tableau;
    tableau = new Pixel_img[hauteur * largeur];
    int i = 0;
    while ( i< hauteur) {
        int j = 0;
        while (j < largeur) {
            fichier >> r >> g >> b;
            Pixel_img pix1(r, g, b);
            setPix(j, i, pix1);
            ++j;
        }
        ++i;
    }
    fichier.close();
    cout << nomFichier ;
}

void Image::afficherConsole()
{
    cout << hauteur << " * " << largeur << endl;
    int i = 0;
    while (i < hauteur) {
        int j = 0;
        while (j < largeur) {
            const Pixel_img &pix = getPix(j, i);
            cout << +pix.rouge << " " << +pix.vert << " " << +pix.bleu << " ";
            ++j
        }
        cout << endl;
        ++i;
    }
}

