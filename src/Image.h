#ifndef PROCONCEPTION_IMAGE_H
#define PROCONCEPTION_IMAGE_H

#include "Pixel.h"

/**
 * @class Image
 * @brief Classe décrivant une représentation graphique composée de pixels de type @ref Pixel.
 */
class Image {
private:
    int hauteur, largeur;
    Pixel * tableau;
public:
    /**
     * @brief Constructeur par défaut initialisant une image vide sans allocation de mémoire.
     */
    Image();

    /**
     * @brief Constructeur permettant de créer une image de dimensions spécifiées avec allocation de mémoire pour les pixels.
     * Les dimensions doivent être strictement positives.
     * @param hauteur_img La hauteur de l'image.
     * @param largeur_img La largeur de l'image.
     */
    Image(int hauteur_img, int largeur_img);

    /**
     * @brief Destructeur libérant la mémoire allouée pour le tableau de pixels.
     */
    ~Image();

    /**
     * @brief Vérifie si les coordonnées spécifiées correspondent à une position valide dans l'image.
     * @param posX La position horizontale.
     * @param posY La position verticale.
     * @return bool Renvoie vrai si les coordonnées sont valides, sinon faux.
     */
    bool estValide(int posX, int posY) const;

    /**
     * @brief Récupère le pixel situé aux coordonnées spécifiées.
     * @param posX La position horizontale.
     * @param posY La position verticale.
     * @return Pixel_img Le pixel aux coordonnées indiquées.
     */
    Pixel_img getPix(int posX, int posY);

    /**
     * @brief Récupère une copie du pixel situé aux coordonnées spécifiées.
     * @param posX La position horizontale.
     * @param posY La position verticale.
     * @return Pixel_img Une copie du pixel aux coordonnées indiquées.
     */
    Pixel_img getPix(int posX, int posY) const;

    /**
     * @brief Modifie le pixel situé aux coordonnées spécifiées.
     * @param posX La position horizontale.
     * @param posY La position verticale.
     * @param color La couleur du pixel à définir.
     */
    void setPix(int posX, int posY, Pixel_img color);

    /**
     * @brief Dessine un rectangle de couleur spécifiée aux positions données.
     * @param posX_min La position horizontale minimale.
     * @param posX_max La position horizontale maximale.
     * @param posY_min La position verticale minimale.
     * @param posY_max La position verticale maximale.
     * @param color La couleur du rectangle à dessiner.
     */
    void dessinerRectangle(int posX_min, int posX_max, int posY_min, int posY_max, Pixel_img color);

    /**
     * @brief Efface l'image en remplissant l'ensemble de ses pixels avec la couleur spécifiée.
     * @param color La couleur d'effacement.
     */
    void effacer(Pixel_img color);

    /**
     * @brief Exécute une série de tests de régression sur la classe Image.
     */
    static void testRegression();
};

#endif //PROCONCEPTION_IMAGE_H
