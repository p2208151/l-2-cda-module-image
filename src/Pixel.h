#ifndef PROCONCEPTION_PIXEL_H
#define PROCONCEPTION_PIXEL_H

/**
 * @struct Pixel
 * @brief Représente un pixel coloré en RVB.
 */
struct Pixel {
    unsigned char r; /**< Composante rouge. */
    unsigned char g; /**< Composante verte. */
    unsigned char b; /**< Composante bleue. */

    /**
     * @brief Constructeur par défaut initialisant le pixel en noir.
     */
    Pixel() : r(0), g(0), b(0) {}

    /**
     * @brief Constructeur par copie initialisant le pixel avec les mêmes valeurs que le pixel passé en paramètre.
     * @param Pixel Le pixel à copier.
     */
    Pixel(const Pixel &pixel) : r(pixel.r), g(pixel.g), b(pixel.b) {}

    /**
     * @brief Constructeur initialisant le pixel avec les valeurs spécifiées pour chaque composante de couleur.
     * @param unsigned char red La composante rouge.
     * @param unsigned char green La composante verte.
     * @param unsigned char blue La composante bleue.
     */
    Pixel(unsigned char red, unsigned char green, unsigned char blue) : r(red), g(green), b(blue) {}
};

#endif //PROCONCEPTION_PIXEL_H

