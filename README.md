# L2_CDA_ModuleImage

Module Image - README

Groupe : FERCHICHE fares (p2208151) / BOUAZDIA rayane (p2212652)

Description : Ce projet, effectué dans le cadre du cours de Conception et Développement d'Applications à l'Université Lyon 1, se concentre sur le développement du module Image, offrant des fonctionnalités avancées de manipulation d'images, avec un dépôt associé sur la plateforme de développement collaboratif de l'université.

Projet : Voici l’organisation de notre projet:

SRC /  Le dossier "src" contient les fichiers sources du module Image, comprenant la classe Image, les structures Pixel, ainsi qu'un fichier principal (main) permettant la compilation du module.

DOC /  Le dossier “doc” correspond à la documentation générée par Doxygen spécifique pour le module Image.

CMAKELISTS / Le fichier "CMakeLists.txt" est une configuration spécifique à CLion, servant de Makefile pour compiler le code du projet.


Compilation : Pour compiler le module Image et ses exécutables associés :

1. Assurez-vous d'avoir les dépendances nécessaires installées, telles que SDL2.
2. Ouvrez le projet dans CLion.
3. Configurez le projet selon vos besoins.
4. Compilez le projet.

ID Projet forget : 33169
